package com.facci.examen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    private static final String URL_P = "http://10.32.6.242:3005/api/profesor/1";
    private static String URL_E = "";
    private ArrayList<Modelo> estudiantesArrayList;
    private Adaptador adaptadorEstudiantes;
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private ImageView imageViewP;
    private TextView identidad, nombres, apellidos, titular, profesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        String id = getIntent().getStringExtra("id");
        estudiantesArrayList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("CARGANDO");
        progressDialog.show();
        adaptadorEstudiantes = new Adaptador(estudiantesArrayList);
        imageViewP = (ImageView)findViewById(R.id.ImagenProfesor);
        identidad = (TextView)findViewById(R.id.LBLIdentidadP);
        nombres = (TextView)findViewById(R.id.LBLNombresP);
        apellidos = (TextView)findViewById(R.id.LBLApellidosP);
        titular = (TextView)findViewById(R.id.LBLTitularP);
        profesion = (TextView)findViewById(R.id.LBLProfesionP);
        URL_E = "http://10.32.6.242:3005/api/profesor/estudiantes/1" + id;
        Profesor(id);
        Estudiante(URL_E);


    }
    private void Estudiante(String URl) {
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.e("Hola", response);
                    for (int i= 0; i<jsonArray.length(); i++){
                        JSONObject object = jsonArray.getJSONObject(i);
                        Modelo estudiantes = new Modelo();
                        estudiantes.setId(object.getString("id"));
                        estudiantes.setApellidos(object.getString("apellidos"));
                        estudiantes.setNombres(object.getString("nombres"));
                        estudiantes.setImagen(object.getString("imagen"));
                        estudiantes.setParcial_uno(object.getString("parcial_uno"));
                        estudiantes.setParcial_dos(object.getString("parcial_dos"));
                        estudiantesArrayList.add(estudiantes);
                    }
                    adaptadorEstudiantes = new Adaptador(estudiantesArrayList);
                    recyclerView.setAdapter(adaptadorEstudiantes);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void Profesor(String id) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_P+ id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    nombres.setText(jsonObject.getString("nombre"));
                    apellidos.setText(jsonObject.getString("apellido"));
                    identidad.setText(jsonObject.getString("identidad"));
                    titular.setText(String.valueOf(jsonObject.getBoolean("titular")));
                    profesion.setText(jsonObject.getString("profesion"));
                    Picasso.get().load(jsonObject.getString("imagen")).into(imageViewP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}