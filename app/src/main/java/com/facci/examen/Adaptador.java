package com.facci.examen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> {
    ArrayList<Modelo> modelo;

    public Adaptador(ArrayList<Modelo> modelo) {
        this.modelo = modelo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Modelo estudiantes = modelo.get(i);
        viewHolder.nombres.setText(estudiantes.getNombres());
        viewHolder.apellidos.setText(estudiantes.getApellidos());
        viewHolder.parcial1.setText(estudiantes.getParcial_uno());
        viewHolder.parcial2.setText(estudiantes.getParcial_dos());
        Picasso.get().load(estudiantes.getImagen()).into(viewHolder.foto);
    }

    @Override
    public int getItemCount() {
        return modelo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombres, apellidos, parcial1, parcial2;
        ImageView foto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nombres = (TextView)itemView.findViewById(R.id.LBLNombresE);
            apellidos = (TextView)itemView.findViewById(R.id.LBLApellidosE);
            parcial1 = (TextView)itemView.findViewById(R.id.LBLParcial1E);
            parcial2 = (TextView)itemView.findViewById(R.id.LBLPArcial2E);
            foto = (ImageView)itemView.findViewById(R.id.ImagenEstudiante);
        }
    }



}
